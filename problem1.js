//function file for problem 1
function problem1(inventory, id){
    if(typeof inventory !== "object"){
        return [];
    }
    if(typeof id !== "number"){
        return [];
    }
    if(inventory.length < 1){
        return [];
    }
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].id === id){
            return inventory[i]
        }
    }
}

module.exports = problem1

