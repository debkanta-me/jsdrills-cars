function problem2(inventory){
    if(typeof inventory !== "object"){
        return [];
    }
    if(inventory.length < 1){
        return [];
    }
    return inventory[inventory.length-1];
    
}

module.exports = problem2;