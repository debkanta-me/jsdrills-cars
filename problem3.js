function problem3(inventory){
    if(typeof inventory !== "object"){
        return [];
    }
    if(inventory.length < 1){
        return [];
    }
    return inventory.sort((a, b) => {
        return a.car_model < b.car_model ? -1 : a.car_model > b.car_model ? 1 : 0;  
    });
}

module.exports = problem3;