function problem5(inventory, years){
    let result = []
    if(typeof inventory !== "object"){
        return [];
    }
    if(inventory.length < 1){
        return [];
    }
    if(typeof years !== "object"){
        return [];
    }
    if(years.length < 1){
        return [];
    }
    for(let i=0;i<years.length;i++){
        if(years[i]<2000){
            result.push(years[i]);
        }
    }

    return result;
}

module.exports = problem5;