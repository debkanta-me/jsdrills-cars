function problem6(inventory){
    let result = []
    if(typeof inventory !== "object"){
        return [];
    }
    if(inventory.length < 1){
        return [];
    }
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].car_make === "BMW" || inventory[i].car_make === "Audi"){
            result.push(inventory[i]);
        }
    }

    return result;
}

module.exports = problem6;