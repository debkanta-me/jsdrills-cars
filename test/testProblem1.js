// test file for problem 1
const inventory = require("../Inventory");
const problem1 = require("../problem1");

const result = problem1(inventory);

if(!Array.isArray(result)){
    console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}else{
    console.log([]);
}