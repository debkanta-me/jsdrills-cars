const inventory = require("../Inventory");
const problem2 = require("../problem2");

const result = problem2();

if(!Array.isArray(result)){
    console.log(`Last car is a ${result.car_make} ${result.car_model}`);
}else{
    console.log([]);
}