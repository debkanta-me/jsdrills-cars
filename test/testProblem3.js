const inventory = require("../Inventory");
const problem3 = require("../problem3");

const result = problem3(inventory);

if(result.length > 1){
    console.log(result);
}else{
    console.log([]);
}