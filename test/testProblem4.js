const inventory = require("../Inventory");
const problem4 = require("../problem4");

const result = problem4(inventory);

if(result.length > 1){
    console.log(result);
}else{
    console.log([]);
}
module.exports = result;