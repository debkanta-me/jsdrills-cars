const inventory = require("../Inventory");
const problem6 = require("../problem6");

const result = problem6(inventory);

if(result.length > 1){
    console.log(JSON.stringify(result));
}else {
    console.log([]);
}